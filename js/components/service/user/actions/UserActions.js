var Dispatcher = require('../../dispatcher/Dispatcher');
var Action = require('../../../../util/Action').Action;

var Constants = require('../Constants');

var UserActions = {
    saveUserDetails: function(userData){
        Dispatcher.dispatch(new Action(Constants.STORE_USER_DETAILS, userData));
    },
    clearUserDetails: function(){
        Dispatcher.dispatch(new Action(Constants.CLEAR_USER_DETAILS));
    }
};

module.exports = UserActions;
