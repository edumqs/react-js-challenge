var sinon = require('sinon');

var Dispatcher = require('../../../dispatcher/Dispatcher');
var Action = require('../../../../../util/Action').Action;

var Constants = require('../../Constants');
var UserActions = require('../UserActions');
var UserStore = require('../../store/UserStore').UserStore;

describe("UserActions.spec.js", function(){

    describe("saveUserDetails(userData)", function(){

        it("should pass the given payload and save action to the dispatcher", sinon.test(function(done){

            try{
                var userDetails = {
                    name: "name",
                    company: "company",
                    serviceAgreementAccepted: true
                };

                this.mock(Dispatcher).expects("dispatch").withExactArgs(new Action(Constants.STORE_USER_DETAILS, userDetails));

                UserActions.saveUserDetails(userDetails);

                done();
            }catch(e){
                done(e);
            }

        }));

        /*
            it.... more tests..

            if we would make ajax request calls in the actions we could also test for example:
            -if the used endpoint URL is correct
            -assert that the dispatcher is not be called when the request returns error
         */

    });

    describe("clearUserDetails()", function(){

        it("should pass the clear_user_details action to the dispatcher", sinon.test(function(done){

            try{

                this.mock(Dispatcher).expects("dispatch").withExactArgs(new Action(Constants.CLEAR_USER_DETAILS));

                UserActions.clearUserDetails();

                done();
            }catch(e){
                done(e);
            }

        }));

    });

});