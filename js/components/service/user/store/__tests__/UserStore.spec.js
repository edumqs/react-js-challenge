var expect = require('chai').expect;

var Constants = require('../../Constants');
var Dispatcher = require('../../../dispatcher/Dispatcher');
var Action = require('../../../../../util/Action').Action;
var UserStoreModule = require('../UserStore');
var UserStore = UserStoreModule.UserStore;

function clearStoreState () {
    UserStoreModule.__set__({
        _state: undefined
    });
}

beforeEach(clearStoreState);

describe("UserStore.spec.js", function(){

    describe("getUserDetails()", function(){

        it("should return undefined if the user details have not been stored", function(){

            expect(UserStore.getUserDetails()).to.be.undefined;

        });

        it("should return the user details saved in the store", function(){

            var userDetails = {
                name: "name",
                company: "company",
                serviceAgreementAccepted: true
            };

            Dispatcher.dispatch(new Action(Constants.STORE_USER_DETAILS, userDetails));

            expect(UserStore.getUserDetails()).to.deep.equal(userDetails);

        });

    });

    describe("CLEAR_USER_DETAILS action", function(){

        it("should clear the store when called", function(){
            var userDetails = {
                name: "name",
                company: "company",
                serviceAgreementAccepted: true
            };

            Dispatcher.dispatch(new Action(Constants.STORE_USER_DETAILS, userDetails));

            expect(UserStore.getUserDetails()).to.deep.equal(userDetails);

            Dispatcher.dispatch(new Action(Constants.CLEAR_USER_DETAILS));

            expect(UserStore.getUserDetails()).to.be.undefined;
        })

    });

});