var React = require('react');

var Router = require('react-router').Router;
var Route = require('react-router').Route;
var IndexRoute = require('react-router').IndexRoute;
var hashHistory = require('react-router').hashHistory;

var render = require('react-dom').render;

var NoMatch = React.createClass({
    render: function(){
        return (
            <h2>Page not found</h2>
        );
    }
});

var routes = (
    <Route path="/" component={require('./js/components/Main.react')}>
        <IndexRoute component={require('./js/components/homepage/Homepage.react')}/>
        <Route path="new-user" component={require('./js/components/new-user/NewUserStep.react')} />
        <Route path="review-user" component={require('./js/components/new-user/ReviewStep.react')} />
        <Route path="final-user" component={require('./js/components/new-user/FinalStep.react')} />
        <Route path="*" component={NoMatch}/>
    </Route>
);

render(<Router history={hashHistory}>{routes}</Router>,document.getElementById('renderDiv'));

