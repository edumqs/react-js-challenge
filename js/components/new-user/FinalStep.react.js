var React = require('react');
var PropTypes = React.PropTypes;

var PanelHeader = require('./PanelHeader.react');
var CreateUser = require('./UserForm.react.js');

var UserActions = require('../service/user/actions/UserActions');
var UserStore = require('../service/user/store/UserStore').UserStore;

var FinalStep = React.createClass({
    getInitialState: function(){
        return{
            userData: UserStore.getUserDetails()
        }
    },
    componentWillMount: function(){
        //if a user tries to access directly this page without adding first a user we redirect him to the NewUser page
        if(!UserStore.getUserDetails()){
            this.context.router.push('new-user');
        }
    },
    componentWillUnmount: function(){
        //once the user leaves this component the data in the store will be invalid
        UserActions.clearUserDetails();
    },
    _onInputChange: function(userData){
        this.setState({
            userData: userData
        });
    },
    _onBackClick: function(){
        this._saveAndRedirect('new-user');
    },
    render: function(){

        if(!this.state.userData){
            return <span />
        }

        return (
            <div className="form-tile new-user">
                <PanelHeader step={"final"} />

                <CreateUser ref={"user-form"} userData={this.state.userData} onUserChange={this._onInputChange} readOnly={true} />

            </div>
        );
    }
});

FinalStep.contextTypes = {
    router: PropTypes.object.isRequired
};

module.exports = FinalStep;