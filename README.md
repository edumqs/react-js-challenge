#React.Js#

### DEMO? ###

-Yes! :) **http://edupi2.noip.me:9000/reactjs**

### How do I get set up? ###

1. Clone this repository
2. **cd ../path-to-this-repo**
3. **npm install** (installs all the required dependencies)

### How to run the application? ###

1. **cd ../path-to-this-repo**
2. **npm run start**
3. Go to your browser and enter http://localhost:8080/ (make sure you don't have anything running on this port)
4. Have fun :)

### How to run the tests? ###

1. **npm run watch-test**


### Considerations ###

The NewUserStep, ReviewStep and FinalStep components are all very similar and we could argue that they could be reused by passing a property indicating what step we would be on, but as we want each step to have its own url I believe that each component should have its own responsability. In a small app like this one they look similar, but at a bigger scale with added functionality for each step the code could become messy.