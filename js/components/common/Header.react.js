var React = require('react');

var Header = React.createClass({
    render: function(){

        return (
            <div className="header">
                <h2><a href="#">React.JS Challenge</a></h2>
            </div>
        );
    }
});

module.exports = Header;