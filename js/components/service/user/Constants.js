var keymirror = require('keymirror');

module.exports = {
    STORE_USER_DETAILS: null,
    CLEAR_USER_DETAILS: null
};

module.exports = (keymirror(module.exports));