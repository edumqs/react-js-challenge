var React = require('react');

var ReactBootstrap = require('react-bootstrap');
var Row = ReactBootstrap.Row;
var Col = ReactBootstrap.Col;

var Header = React.createClass({
    propTypes: {
        step: React.PropTypes.string.isRequired
    },
    render: function(){
        return (
            <div className="panel-header">
                <Row>
                    <Col xs={4}>
                        <div className={this.props.step == "new" ? "selected" : ""}>
                            <span>Form</span>
                        </div>
                    </Col>
                    <Col xs={4}>
                        <div className={this.props.step == "review" ? "selected" : ""}>
                            <span>Review</span>
                        </div>
                    </Col>
                    <Col xs={4}>
                        <div className={this.props.step == "final" ? "selected" : ""}>
                            <span >Final</span>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
});

module.exports = Header;