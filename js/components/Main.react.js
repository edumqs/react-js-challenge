var React = require('react');

var ReactBootstrap = require('react-bootstrap');
var Row = require('react-bootstrap').Row;
var Col = require('react-bootstrap').Col;
var Panel = ReactBootstrap.Panel;

var Header = require('./common/Header.react');

module.exports = React.createClass({

    render: function(){
        return (
            <div className="top-component">
                <Header/>
                <Row>
                    <Col sm={12}>
                        <div className="content-body">
                            {this.props.children}
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
});