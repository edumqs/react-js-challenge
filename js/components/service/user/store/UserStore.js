var Dispatcher = require('../../dispatcher/Dispatcher');
var EventEmitter = require('events').EventEmitter;
var Constants = require('../Constants');
var assign = require('object-assign');
var _ = require('underscore');

var CHANGE_EVENT = 'change';

var _state = undefined;

var getState =  function (){
    _state =  _state || {};
    return _state;
};

var UserStore = assign({}, EventEmitter.prototype, {
    getUserDetails: function(){
        return getState().userDetails;
    },
    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },
    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

// Register callback to handle all updates
Dispatcher.register(function(action) {

    switch(action.actionType) {

        case Constants.STORE_USER_DETAILS:

            getState().userDetails = action.payload;

            UserStore.emitChange();
            break;
        case Constants.CLEAR_USER_DETAILS:

            getState().userDetails = undefined;

            UserStore.emitChange(); // just the components listening to this store will receive updates
            break;
        default:
            break;
    }
});

module.exports = {
    UserStore: UserStore
};