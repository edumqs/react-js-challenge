var React = require('react');
var PropTypes = React.PropTypes;

var ReactBootstrap = require('react-bootstrap');
var Button = ReactBootstrap.Button;
var Row = ReactBootstrap.Row;
var Col = ReactBootstrap.Col;

var PanelHeader = require('./PanelHeader.react');
var UserForm = require('./UserForm.react.js');

var UserActions = require('../service/user/actions/UserActions');
var UserStore = require('../service/user/store/UserStore').UserStore;

var ReviewUser = React.createClass({
    getInitialState: function(){
        return{
            userData: UserStore.getUserDetails()
        }
    },
    componentWillMount: function(){
        //if a user tries to access directly this page without adding first a user we redirect him to the NewUser page
        if(!UserStore.getUserDetails()){
            this.context.router.push('new-user');
        }
    },
    _onInputChange: function(userData){
        this.setState({
            userData: userData
        });
    },
    _onBackClick: function(){
        this._saveAndRedirect('new-user');
    },
    _onSubmitClick: function(){
        if(!this.refs['user-form'].isFormValid())
            return;

        this._saveAndRedirect('final-user');
    },
    _saveAndRedirect: function(page){
        this.context.router.push(page);
        UserActions.saveUserDetails(this.state.userData);
    },
    render: function(){

        if(!this.state.userData){
            return <span />
        }

        return (
            <div className="form-tile new-user">
                <PanelHeader step={"review"} />

                <UserForm ref={"user-form"} userData={this.state.userData} onUserChange={this._onInputChange} hideServiceAgreement />

                <Row className="buttons">
                    <Col md={6}>
                        <Button onClick={this._onBackClick}>Back</Button>
                    </Col>
                    <Col md={6} className="align-right">
                        <Button onClick={this._onSubmitClick}>Submit</Button>
                    </Col>
                </Row>
            </div>
        );
    }
});

ReviewUser.contextTypes = {
    router: PropTypes.object.isRequired
};

module.exports = ReviewUser;