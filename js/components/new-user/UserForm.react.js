var React = require('react');
var ReactDOM = require('react-dom');
var assign = require('object-assign');

var ReactBootstrap = require('react-bootstrap');
var FormGroup = ReactBootstrap.FormGroup;
var FormControl = ReactBootstrap.FormControl;
var ControlLabel = ReactBootstrap.ControlLabel;
var Checkbox = ReactBootstrap.Checkbox;
var Row = ReactBootstrap.Row;
var Col = ReactBootstrap.Col;

var Switch = require('../switch/SwitchBootstrap.react');
var DisplayValidation = require('./DisplayValidation.react');

var UserForm = React.createClass({
    propTypes: {
        userData: React.PropTypes.object,
        onUserChange: React.PropTypes.func.isRequired,
        hideServiceAgreement: React.PropTypes.bool,
        readOnly: React.PropTypes.bool
    },
    getInitialState: function(){
        return {
            validate: false
        }
    },
    /*
        function ifFormValid():
        The parent component will use the isFormValid function to verify if this form is valid
        -If valid returns true
        -If not valid returns false and updates the state of this component to show the errors to the user

        *Ideally we should have another component taking care of the validation, it would wrap
        * the whole form and the individual inputs not allowing the user to submit it if
        * the validation criteria would not be met, but for the purpose of this challenge I will keep it simple
     */
    isFormValid: function(){
        /*
         As the validation criteria should be defined on the input level the parent
         component does not need to know the validation status for each input leaving that
         responsibility to the component that renders the form.
         For this challenge I will use a simple 'if' to verify if the input values are valid
        */
        if(this.props.userData.name.length > 0 && this.props.userData.company.length > 0 && this.props.userData.serviceAgreementAccepted == true){
            return true;
        }

        this.setState({
            validate: true
        });

        return false;
    },
    _handleInputChange: function(inputName, e){
        var value = e.target.value;

        var formData = assign({}, this.props.userData);
        formData[inputName] = value;

        this.props.onUserChange(formData);
    },
    _handleToggleChange: function(value){

        var formData = assign({}, this.props.userData);
        formData["serviceAgreementAccepted"] = value;

        this.props.onUserChange(formData);
    },
    _renderServiceAgreement: function(){
        if(this.props.hideServiceAgreement){
            return <span/>;
        }

        return (
            <DisplayValidation id="USER_SERVICE_AGREEMENT_INPUT" isValidating={this.state.validate} valid={this.props.userData.serviceAgreementAccepted == true ? true : false} message="*To continue you must agree with the terms and conditions">
                <Row className="user-agreement-row">
                    <Col xs={8}>
                        <span>I agree to the terms of service</span>
                    </Col>

                    <Col xs={4} className="align-right">
                        <Switch
                            onInputChange={this._handleToggleChange}
                            defaultChecked={this.props.userData.serviceAgreementAccepted}
                            disabled={this.props.readOnly ? this.props.readOnly : false}
                        />
                    </Col>

                </Row>
            </DisplayValidation>
        );
    },
    render: function(){

        return (
            <div className="user-form">

                <DisplayValidation id="USER_NAME_INPUT" isValidating={this.state.validate} valid={this.props.userData.name.length > 0 ? true : false} message="*This field can not be empty">

                    <FormGroup controlId={"form-user-name"}>
                        <ControlLabel className="input-labels">{"Name"}</ControlLabel>
                        <FormControl
                            className="form-text-input"
                            type="text"
                            placeholder="Enter your name"
                            onChange={this._handleInputChange.bind(this, "name")}
                            value={this.props.userData.name}
                            disabled={this.props.readOnly ? this.props.readOnly : false}
                        />
                    </FormGroup>

                </DisplayValidation>

                <DisplayValidation id="USER_COMPANY_INPUT" isValidating={this.state.validate} valid={this.props.userData.company.length > 0 ? true : false} message="*This field can not be empty">

                    <FormGroup controlId={"form-user-company"}>
                        <ControlLabel className="input-labels">{"Company"}</ControlLabel>
                        <FormControl
                            className="form-text-input"
                            type="text"
                            placeholder="Enter your company"
                            onChange={this._handleInputChange.bind(this, "company")}
                            value={this.props.userData.company}
                            disabled={this.props.readOnly ? this.props.readOnly : false}
                        />
                    </FormGroup>

                </DisplayValidation>

                {this._renderServiceAgreement()}

            </div>
        );
    }
});

module.exports = UserForm;