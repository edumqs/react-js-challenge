var React = require('react');
var ReactDOM = require('react-dom');
var $ = window.$ || require('jquery');

var SwitchBootstrap = React.createClass({
    propTypes: {
        defaultChecked: React.PropTypes.bool.isRequired,
        onInputChange: React.PropTypes.func.isRequired,
        disabled: React.PropTypes.bool.isRequired
    },
    componentDidMount: function(){

        var inputNode = ReactDOM.findDOMNode(this);

        $(inputNode).bootstrapSwitch();

        $(inputNode).on('switchChange.bootstrapSwitch', function(event, state) {
            this.props.onInputChange(state);
        }.bind(this));
    },
    render: function(){

        return (
            <input
                type="checkbox"
                data-on-text="Yes"
                data-off-text="No"
                data-size="mini"
                defaultChecked={this.props.defaultChecked}
                disabled={this.props.disabled}
            />
        );
    }
});

module.exports = SwitchBootstrap;