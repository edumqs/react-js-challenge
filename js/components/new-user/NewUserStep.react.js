var React = require('react');
var PropTypes = React.PropTypes;

var ReactBootstrap = require('react-bootstrap');
var Button = ReactBootstrap.Button;

var PanelHeader = require('./PanelHeader.react');
var CreateUser = require('./UserForm.react.js');

var UserActions = require('../service/user/actions/UserActions');
var UserStore = require('../service/user/store/UserStore').UserStore;

var NewUser = React.createClass({
    getInitialState: function(){
        return{
            userData: UserStore.getUserDetails() || {
                name: '',
                company: '',
                serviceAgreementAccepted: false
            }
        }
    },
    _onInputChange: function(userData){

        this.setState({
            userData: userData
        });
    },
    _onReview: function(){
        if(!this.refs['user-form'].isFormValid())
            return;

        UserActions.saveUserDetails(this.state.userData);

        this.context.router.push('review-user');
    },
    render: function(){
        return (
            <div className="form-tile new-user">
                <PanelHeader step={"new"} />

                <CreateUser ref={"user-form"} userData={this.state.userData} onUserChange={this._onInputChange} />

                <div className="buttons align-right">
                    <Button onClick={this._onReview}>Review</Button>
                </div>
            </div>
        );
    }
});

NewUser.contextTypes = {
    router: PropTypes.object.isRequired
};

module.exports = NewUser;