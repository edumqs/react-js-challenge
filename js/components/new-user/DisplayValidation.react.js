var React = require('react');

var DisplayValidation = React.createClass({
    propTypes: {
        id: React.PropTypes.string.isRequired,
        isValidating: React.PropTypes.bool.isRequired,
        valid: React.PropTypes.bool,
        message: React.PropTypes.string
    },
    render: function(){
        if(this.props.isValidating && !this.props.valid){

            return (
                <div key={this.props.id} className="invalid-field">
                    {this.props.children}
                    <p>{this.props.message}</p>
                </div>
            );
        }

        return (
            <div key={this.props.id}>
                    {this.props.children}
            </div>
        );
    }
});

module.exports = DisplayValidation;