var React = require('react');
var PropTypes = React.PropTypes;

var ReactBootstrap = require('react-bootstrap');
var Button = ReactBootstrap.Button;

var UserStore = require('../service/user/store/UserStore').UserStore;

var Homepage = React.createClass({
    _createNewUser: function(){
        this.context.router.push('new-user');
    },
    _renderCreateUserButton: function(){

        if(UserStore.getUserDetails()){
            return (
                <div className="resume-creating-user">
                    <p>*You are in the middle of creating a user</p>
                    <Button onClick={this._createNewUser}>Resume Creating User</Button>
                </div>
            );
        }

        return <Button onClick={this._createNewUser}>Create New User</Button>;
    },
    render: function(){

        return (
            <div className="homepage">

                {this._renderCreateUserButton()}

            </div>
        );
    }
});

Homepage.contextTypes = {
    router: PropTypes.object.isRequired
};

module.exports = Homepage;